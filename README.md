**UK Car Tax Calculator** - Open Source

This is a public and open source UK Car Tax calculation class and web site.

It integrates a database of car emissions which are available for anyones use and are used for the calculation of UK car tax.

You can use this class (in conjunction with the example site source code) as a stand alone car tax calculator for your own site, or you can add the class to an existing project to integrate car tax calculation into your site.

### What is this repository for? ###

* Quick summary
* Version 0.1 Initial Work 

### How do I get set up? ###

This will depend on how you are using the class.

1) Stand alone site

2) Integrate the Calss

3) API

* Dependencies
MySQL

* Database configuration

* How to run tests

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
Pete Siviter

* Other community or team contact
Adam MAhmood